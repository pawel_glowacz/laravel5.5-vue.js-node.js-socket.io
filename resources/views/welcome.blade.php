<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Laravel</title>
        <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">

    </head>
    <body>
    <div id="app">

        <h1>New users</h1>
        <ul>
            <li v-for="user in users">@{{ user }}</li>
        </ul>
    </div>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/socket.io/2.0.4/socket.io.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/vue/2.5.13/vue.js"></script>

    <script>
        var socket = io('http://localhost:3000');

        new Vue({
            el:'#app',
            data:{
                users:[]
            },
            mounted: function(){
                socket.on('test-channel:App\\Events\\UserSignedUp',function(data){
                    console.log(data);
                    this.users.push(data.username);
                }.bind(this));
            }

        })
    </script>
    </body>
</html>
