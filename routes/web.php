<?php
use Illuminate\Support\Facades\Redis;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
//    $data = [
//        'event' => 'UserSignedUp',
//        'data'  => [
//            'username'  =>  'JohnDoe'
//        ]
//    ];
//    Redis::publish('test-channel',json_encode($data));
//    return "done";
//    Redis::set('name','Pawel');
//    return Redis::get('name');
    event(new \App\Events\UserSignedUp('JohnDoe'));
    return view('welcome');
});
